package com.mateuszjanczak.clock;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class TimeActivity extends AppCompatActivity {

    TextView systemTime;
    TextView newYorkTime;
    TextView londonTime;
    TextView tokyoTime;

    TimeZone Default = TimeZone.getDefault();
    TimeZone NewYork = TimeZone.getTimeZone("America/New_York");
    TimeZone London = TimeZone.getTimeZone("Europe/London");
    TimeZone Tokyo = TimeZone.getTimeZone("Asia/Tokyo");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        systemTime = findViewById(R.id.system_time);
        newYorkTime = findViewById(R.id.newyork_time);
        londonTime = findViewById(R.id.london_time);
        tokyoTime = findViewById(R.id.tokyo_time);

        setTime();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                setTime();
                handler.postDelayed(this, 1000);
            }
        }, 1000);
    }

    private void setTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
        Calendar cal = Calendar.getInstance();

        systemTime.setText(dateFormat.format(cal.getTime()));

        dateFormat.setTimeZone(NewYork);
        newYorkTime.setText(dateFormat.format(cal.getTime()));

        dateFormat.setTimeZone(London);
        londonTime.setText(dateFormat.format(cal.getTime()));

        dateFormat.setTimeZone(Tokyo);
        tokyoTime.setText(dateFormat.format(cal.getTime()));

    }


}
